package app.ws.alumno.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.ws.alumno.dao.api.alumnoRepository;
import app.ws.alumno.dao.api.student_courseRepository;
import app.ws.alumno.model.ResponseAlumnosCurso;
import app.ws.alumno.model.student_course;
import app.ws.alumno.model.users;
import app.ws.alumno.service.api.alumnoService;


@Service
public class alumnoServiceImpl implements alumnoService{

	@Autowired
	private alumnoRepository aRepository;
	
	@Autowired
	private student_courseRepository scRepository;
	
	public List<ResponseAlumnosCurso> getAlumnosCurso(ObjectId idCurso){	
		List<ResponseAlumnosCurso> lista =  new ArrayList<ResponseAlumnosCurso>();

		List<student_course> students_course = scRepository.getAlumnosCurso(idCurso);
		
		for(int i=0; i<students_course.size();i++) {
			ResponseAlumnosCurso response = new ResponseAlumnosCurso();
			
			
			ObjectId idAlumno = new ObjectId(students_course.get(i).getStudent().toString(16));


			users user = aRepository.getAlumno(idAlumno);
			ObjectId idRole = new ObjectId(user.getRole().toString(16));
			ObjectId idDocumentType = new ObjectId(user.getDocumenttype().toString(16));
			ObjectId idStatus = new ObjectId(user.getStatus().toString(16));

			
			response.setStudent(idAlumno.toString());
			response.setName(user.getName());
			response.setLastname(user.getLastname());
			response.setBirthdate(user.getBirthdate());
			response.setEmail(user.getEmail());
			response.setRole(idRole.toString());
			response.setDocumentnumber(user.getDocumentnumber());
			response.setDocumenttype(idDocumentType.toString());
			response.setStatus(idStatus.toString());
			response.setScore(students_course.get(i).getScores());
			
			
			lista.add(response);
		}
		
		return lista;
	}
	
	public users getAlumno(ObjectId idAlumno){	
		//6263144e104de08d078d699b
		users alumno = aRepository.getAlumno(idAlumno);
		return alumno;
	}
}
