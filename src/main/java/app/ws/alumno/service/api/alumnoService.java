package app.ws.alumno.service.api;
import java.util.List;

import org.bson.types.ObjectId;

import app.ws.alumno.model.ResponseAlumnosCurso;
import app.ws.alumno.model.users;

public interface alumnoService{	
	public List<ResponseAlumnosCurso> getAlumnosCurso(ObjectId idCurso);
	
	public users getAlumno(ObjectId idAlumno);
}
