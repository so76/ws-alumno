package app.ws.alumno.controller;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.ws.alumno.service.api.alumnoService;
import app.ws.alumno.model.ResponseAlumnosCurso;

@RestController
@RequestMapping("/alumno")
public class alumnoController {
	
	@Autowired
	private alumnoService aService;
	
	@RequestMapping("/alumnos_curso/{idcurso}")
	public List<ResponseAlumnosCurso> getAlumnosCurso(Model model,@PathVariable("idcurso") ObjectId idcurso ) {
		return aService.getAlumnosCurso(idcurso);
	}
	
}
