package app.ws.alumno;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlumnoWsApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlumnoWsApplication.class, args);
	}

}
