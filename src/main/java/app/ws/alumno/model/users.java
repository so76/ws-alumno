package app.ws.alumno.model;

import java.math.BigInteger;
import java.sql.Date;

import org.springframework.data.annotation.Id;


public class users {
	@Id
	private BigInteger id;
	private String name;
	private String lastname;
	private Date birthdate;
	private BigInteger documenttype;
	private String documentnumber;
	private String email;
	private String password;
	private BigInteger role;
	private BigInteger status;
	
	public users(BigInteger id, String name, String lastname, Date birthdate, BigInteger documenttype, String documentnumber,
			String email, String password, BigInteger role, BigInteger status) {
		super();
		this.id = id;
		this.name = name;
		this.lastname = lastname;
		this.birthdate = birthdate;
		this.documenttype = documenttype;
		this.documentnumber = documentnumber;
		this.email = email;
		this.password = password;
		this.role = role;
		this.status = status;
	}
	
	public users() {
	}

	public BigInteger getId() {
		return id;
	}
	public void setId(BigInteger id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public Date getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}
	public BigInteger getDocumenttype() {
		return documenttype;
	}
	public void setDocumenttype(BigInteger documenttype) {
		this.documenttype = documenttype;
	}
	public String getDocumentnumber() {
		return documentnumber;
	}
	public void setDocumentnumber(String documentnumber) {
		this.documentnumber = documentnumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public BigInteger getRole() {
		return role;
	}
	public void setRole(BigInteger rol) {
		this.role = rol;
	}
	public BigInteger getStatus() {
		return status;
	}
	public void setStatus(BigInteger status) {
		this.status = status;
	}

	
}
