package app.ws.alumno.model;

import java.math.BigInteger;
import java.util.Date;

public class score {
	private BigInteger id;
	private String name;
	private Date day;
	private int score;
	
	public score() {
		
	}
	public score(BigInteger id, String name, Date day, int score) {
		super();
		this.id = id;
		this.name = name;
		this.day = day;
		this.score = score;
	}
	public BigInteger getId() {
		return id;
	}
	public void setId(BigInteger id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getDay() {
		return day;
	}
	public void setDay(Date day) {
		this.day = day;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}

}
