package app.ws.alumno.model;

import java.sql.Date;
import java.util.ArrayList;

public class ResponseAlumnosCurso {
	private String student;
	private String name;
	private String lastname;
	private Date birthdate;
	private String documenttype;
	private String documentnumber;
	private String email;
	private String role;
	private String status;
	private ArrayList<score> scores;
	
	public ResponseAlumnosCurso() {
		
	}
	
	public ResponseAlumnosCurso(String student, String name, String lastname, Date birthdate, String documenttype,
			String documentnumber, String email, String role, String status,
			ArrayList<app.ws.alumno.model.score> scores) {
		super();
		this.student = student;
		this.name = name;
		this.lastname = lastname;
		this.birthdate = birthdate;
		this.documenttype = documenttype;
		this.documentnumber = documentnumber;
		this.email = email;
		this.role = role;
		this.status = status;
		this.scores = scores;
	}
	public String getStudent() {
		return student;
	}
	public void setStudent(String id) {
		this.student = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public Date getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}
	public String getDocumenttype() {
		return documenttype;
	}
	public void setDocumenttype(String documenttype) {
		this.documenttype = documenttype;
	}
	public String getDocumentnumber() {
		return documentnumber;
	}
	public void setDocumentnumber(String documentnumber) {
		this.documentnumber = documentnumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public ArrayList<score> getScore() {
		return scores;
	}
	public void setScore(ArrayList<score> scores) {
		this.scores = scores;
	}
	
	
}
