package app.ws.alumno.model;

import java.math.BigInteger;
import java.util.ArrayList;

import org.springframework.data.annotation.Id;

public class student_course {
	@Id
	private BigInteger id;
	private BigInteger student;
	private BigInteger course;
	private ArrayList<score> scores;
	private BigInteger status;
	
	public student_course() {}
	
	public student_course(BigInteger id, BigInteger student, BigInteger course, ArrayList<score> scores,
			BigInteger status) {
		super();
		this.id = id;
		this.student = student;
		this.course = course;
		this.scores = scores;
		this.status = status;
	}
	public BigInteger getId() {
		return id;
	}
	public void setId(BigInteger id) {
		this.id = id;
	}
	public BigInteger getStudent() {
		return student;
	}
	public void setStudent(BigInteger student) {
		this.student = student;
	}
	public BigInteger getCourse() {
		return course;
	}
	public void setCourse(BigInteger course) {
		this.course = course;
	}
	public ArrayList<score> getScores() {
		return scores;
	}
	public void setScores(ArrayList<score> scores) {
		this.scores = scores;
	}
	public BigInteger getStatus() {
		return status;
	}
	public void setStatus(BigInteger status) {
		this.status = status;
	}
	
	
}
