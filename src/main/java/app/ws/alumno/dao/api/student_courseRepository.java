package app.ws.alumno.dao.api;

import java.math.BigInteger;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import app.ws.alumno.model.student_course;

public interface student_courseRepository  extends MongoRepository<student_course, BigInteger> {
	
	@Query("{ 'course': ?0}")
	List<student_course> getAlumnosCurso(final ObjectId idCurso);
}
