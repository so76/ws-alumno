package app.ws.alumno.dao.api;

import java.math.BigInteger;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import app.ws.alumno.model.users;

public interface alumnoRepository extends MongoRepository<users, BigInteger>{
	
	@Query("{ '_id': ?0}")
	users getAlumno(final ObjectId idAlumno);
	
}
